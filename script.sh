wpbak(){
	cd $1
  #Changes Directory into your docroot
	wp db export&
  #In the background. Per wp cli "Runs mysqldump utility using DB_HOST, DB_NAME, DB_USER and DB_PASSWORD database credentials specified in wp-config.php."
	wp config list --skip-{plugins,themes} >> "wp-config.$(date '+%Y-%m-%d')"
  #Lists variables, constants, and file includes defined in wp-config.php file. drops that in a timestamped file  
	cd ..
  #Hops one level up
	tar -zcvf "$1.$(date '+%Y-%m-%d')tar.gz" $1&
  #Starts zipping, then amends a datestamp to the end of your targz, in the background
	}


##cleaner version
wpbak(){
cd $1
wp db export
wp config list --skip-{plugins,themes} >> "wp-config.$(date '+%Y-%m-%d')"
cd ..
tar -zcvf "$1.$(date '+%Y-%m-%d')tar.gz" $1&
}
